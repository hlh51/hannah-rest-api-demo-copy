from typing import Union
from pydantic import BaseModel
from fastapi import FastAPI, Request

import os
import datetime

app = FastAPI()

#how to connect a post and get so that data sent by post from user can be received by admin - today's goal
#how do we get the info sent with post to "exist" in api so everyone can retrieve it using get? - list = [] #later will be using actual database

#public interface fastapi commands
info = []
@app.post("/polypeptide")
async def gather(request: Request):
    #sending polypeptide info to fastapi for engine to collect
    res = await request.body()
    info.clear() #clears any info from previous post commands
    info.append(res)
    return res

data = []
@app.post("/database")
async def collect(request: Request):
    #sending database info to fastapi for admin to collect
    res = await request.body()
    data.clear() #clears any data from previous post commands
    data.append(res)
    return res

result = []
@app.get("/result")
def result():
    return result

#checking to see if admin and engine can get info and data needed from public interface

@app.get("/admin")
def admin():
    return data[0]

@app.get("/engine")
def engine():
    return info[0]


#basic endpoints created by mark

@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.get("/uptime")
def return_uptime():
    awake_since = os.popen('uptime -p').read()
    right_now = datetime.datetime.now()
    return {"server_uptime": awake_since,
            "current_timestamp": right_now}

@app.post("/polypeptide/{peptide_string}")
def read_peptide(peptide: Union[str, None] = None):
    return {"peptide": peptide}


#messing around with post in early stages

class QueryInput(BaseModel):
    query: str

@app.post("/submit-query")
async def submit_query(query_input: QueryInput):
    user_query = query_input.query
    return {"received_query": user_query}
